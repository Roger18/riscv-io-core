# Low-Latency I/O Risc-V core on BeagleV-Fire

## Introduction
This repository is a fork of BeagleV-Fire Gateware created to track progress of GSOC project to implement a Low latency I/O RISC-V core on BeagleV-Fire's FPGA Fabric.
This core will provide high bandwidth between the CPU and I/O, resulting in a on-board microcontroller.

The BeagleV Fire gateware builder is a Python script that builds both the PolarFire SoC HSS bootloader and Libero FPGA project into a single programming bitstream. It uses a list of repositories/branches specifying the configuration of the BeagleV Fire to build.

## Important Links
- Weekly update forum link: [here](https://forum.beagleboard.org/t/weekly-progress-report-low-latency-i-o-risc-v-cpu-core-in-fpga-fabric/38488)
- Project Proposal: [](https://gsoc-beagleboard-io-roger18-540382483fe38964961413f637fd07bdbda.beagleboard.io/proposals/RISC-V_CPU_core_in_FPGA_fabric.html)

## Files 
- Mem-mapped-IO: This entry denotes working of memory mapped IO using a standard RISC-V core and a C program file to be flash onto the soft core.
- Reg-mapped-IO: This entry denotes working of Register mapped IO using a standard RISC-V core and a C program file to be flash onto the soft core.
- Pico-Inst: This entry denotes register mapped IO working with picoRV32 soft core.
### Memory map for PICORV

| Address range          | Description                 |
|:----------------------:|:----------------------------|
| 0000_0000 -> 000F_FFFF | Program Memory (1Mb)        |
| 0010_0000 -> 0010_0FFF | Data Memory (4Kb)           |
| 0010_1000 -> 0100_1FFF | Stack Memory (4Kb)          |
| 0010_2000              | GPIO Read register          |
| 0010_2004              | GPIO Output Enable Register |
| 0010_2008              | GPIO Output Register        |

## Prerequisites
### Python libraries
The following Python libraries are used:
- GitPython
- PyYAML

```
pip3 install gitpython
pip3 install pyyaml
```

### Microchip Tools
The SoftConsole and Libero tools from Microchip are required by the bitstream builder.

The following environment variables are required for the bitstream builder to use the Microchip tools:
- SC_INSTALL_DIR
- FPGENPROG
- LIBERO_INSTALL_DIR
- LM_LICENSE_FILE

An example script for setting up the environment is available [here](https://openbeagle.org/beaglev-fire/Microchip-FPGA-Tools-Setup). 

## Usage

```
python3 build-bitstream.py <YAML Configuration File>
```

For example, the following command will be build the default beagleV Fire configuration:
```
python3 build-bitstream.py ./build-options/default.yaml
```


### YAML Configuration Files
The YAML configuration files are located in the "build-options" directory.

| Configuration File | Description                                                |
| ------------------ | ---------------------------------------------------------- |
| default.yaml       | Default gateware including default cape and M.2 interface. |
| minimal.yaml       | Minimal Linux system including Ethernet. No FPGA gateware. |
| robotics.yaml      | Similar to default but supporting the Robotics cape.       |

## Supported Platforms
The BeagleV Fire gateware builder has been tested on Ubuntu 20.04.

## Microchip bitstream-builder
The BeagleV-Fire gateware builder is derived from [Microchip's bitstream-builder ](https://github.com/polarfire-soc/icicle-kit-minimal-bring-up-design-bitstream-builder). We recommend that you use either of these scripts as a starting point for your own PolarFire SoC FPGA designs as opposed to using Libero in isolation.