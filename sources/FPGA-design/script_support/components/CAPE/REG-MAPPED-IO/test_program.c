#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#define GPIO_INPUT                 (* (volatile uint32_t * ) 0x03000000)         //read only register
#define GPIO_ENABLE_OUTPUT         (* (volatile uint32_t * ) 0x03000004)         //enable the output pins
#define GPIO_OUTPUT                (* (volatile uint32_t * ) 0x03000008)         //write outputs here
// main function
int main() {
        // Address value of variables are updated for RISC-V Implementation
        GPIO_ENABLE_OUTPUT = 2047;      //enables first 10 output pin
        GPIO_OUTPUT = 31;                //writes 1 at at 1-5 output pin
    return 0;
}

