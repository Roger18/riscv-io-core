
// instr_mem.v - instruction memory for single-cycle RISC-V CPU

module instr_mem #(parameter DATA_WIDTH = 32, ADDR_WIDTH = 32, MEM_SIZE = 512) (
    input       [ADDR_WIDTH-1:0] instr_addr,
    output      [DATA_WIDTH-1:0] instr
);

// array of 64 32-bit words or instructions
reg [DATA_WIDTH-1:0] instr_ram [0:MEM_SIZE-1];

initial begin
    instr_ram[0] = 32'h02000117;
    instr_ram[1] = 32'h10010113;
    instr_ram[2] = 32'h02000197;
    instr_ram[3] = 32'h7F818193;
    instr_ram[4] = 32'h0040006F;
    instr_ram[5] = 32'h0C4002EF;
    instr_ram[6] = 32'h02000537;
    instr_ram[7] = 32'h00000613;
    instr_ram[8] = 32'h18000593;
    instr_ram[9] = 32'h00050513;
    instr_ram[10] = 32'h110000EF;
    instr_ram[11] = 32'h02000537;
    instr_ram[12] = 32'h00000593;
    instr_ram[13] = 32'h00000613;
    instr_ram[14] = 32'h00050513;
    instr_ram[15] = 32'h120000EF;
    instr_ram[16] = 32'h02000537;
    instr_ram[17] = 32'h00050513;
    instr_ram[18] = 32'h130000EF;
    instr_ram[19] = 32'h00000593;
    instr_ram[20] = 32'h00000513;
    instr_ram[21] = 32'h008000EF;
    instr_ram[22] = 32'h0000006F;
    instr_ram[23] = 32'h030007B7;
    instr_ram[24] = 32'h7FF00713;
    instr_ram[25] = 32'h00E7A223;
    instr_ram[26] = 32'h030007B7;
    instr_ram[27] = 32'h01F00713;
    instr_ram[28] = 32'h00E7A423;
    instr_ram[29] = 32'h00000513;
    instr_ram[30] = 32'h00008067;
    instr_ram[31] = 32'hFC010113;
    instr_ram[32] = 32'h00000313;
    instr_ram[33] = 32'h01B12623;
    instr_ram[34] = 32'h00C0006F;
    instr_ram[35] = 32'hFC010113;
    instr_ram[36] = 32'hFF000313;
    instr_ram[37] = 32'h01A12823;
    instr_ram[38] = 32'h01912A23;
    instr_ram[39] = 32'h01812C23;
    instr_ram[40] = 32'h01712E23;
    instr_ram[41] = 32'h00C0006F;
    instr_ram[42] = 32'hFC010113;
    instr_ram[43] = 32'hFE000313;
    instr_ram[44] = 32'h03612023;
    instr_ram[45] = 32'h03512223;
    instr_ram[46] = 32'h03412423;
    instr_ram[47] = 32'h03312623;
    instr_ram[48] = 32'h03212823;
    instr_ram[49] = 32'h02912A23;
    instr_ram[50] = 32'h02812C23;
    instr_ram[51] = 32'h02112E23;
    instr_ram[52] = 32'h40610133;
    instr_ram[53] = 32'h00028067;
    instr_ram[54] = 32'hFF010113;
    instr_ram[55] = 32'h01212023;
    instr_ram[56] = 32'h00912223;
    instr_ram[57] = 32'h00812423;
    instr_ram[58] = 32'h00112623;
    instr_ram[59] = 32'h00028067;
    instr_ram[60] = 32'h00C12D83;
    instr_ram[61] = 32'h01010113;
    instr_ram[62] = 32'h00012D03;
    instr_ram[63] = 32'h00412C83;
    instr_ram[64] = 32'h00812C03;
    instr_ram[65] = 32'h00C12B83;
    instr_ram[66] = 32'h01010113;
    instr_ram[67] = 32'h00012B03;
    instr_ram[68] = 32'h00412A83;
    instr_ram[69] = 32'h00812A03;
    instr_ram[70] = 32'h00C12983;
    instr_ram[71] = 32'h01010113;
    instr_ram[72] = 32'h00012903;
    instr_ram[73] = 32'h00412483;
    instr_ram[74] = 32'h00812403;
    instr_ram[75] = 32'h00C12083;
    instr_ram[76] = 32'h01010113;
    instr_ram[77] = 32'h00008067;
    instr_ram[78] = 32'h00050313;
    instr_ram[79] = 32'h00060E63;
    instr_ram[80] = 32'h00058383;
    instr_ram[81] = 32'h00730023;
    instr_ram[82] = 32'hFFF60613;
    instr_ram[83] = 32'h00130313;
    instr_ram[84] = 32'h00158593;
    instr_ram[85] = 32'hFE0616E3;
    instr_ram[86] = 32'h00008067;
    instr_ram[87] = 32'h00050313;
    instr_ram[88] = 32'h00060A63;
    instr_ram[89] = 32'h00B30023;
    instr_ram[90] = 32'hFFF60613;
    instr_ram[91] = 32'h00130313;
    instr_ram[92] = 32'hFE061AE3;
    instr_ram[93] = 32'h00008067;
    instr_ram[94] = 32'h00050213;
    instr_ram[95] = 32'h00008067;
end

// word-aligned memory access
// combinational read logic
assign instr = instr_ram[instr_addr[31:2]];

endmodule
