module IO_controller #(parameter ADDR_WIDTH = 32)(
    input                       clk, wr_en,
    input      [ADDR_WIDTH-1:0] wr_addr, 
    input      [31:0]           wr_data,
    input      [27:0]           GPIO_IN,
    output reg [27:0]           GPIO_OE,
    output reg [27:0]           GPIO_OUT,
    output     [31:0]           IO_READ_DATA
);
wire [31:0] IO_read;

assign IO_read = {4'b0, GPIO_IN};
assign IO_READ_DATA = IO_read;

always @(*) begin
    if (wr_en) begin
        if (wr_addr == 32'h03000004) GPIO_OE  <= wr_data[27:0] ;
        if (wr_addr == 32'h03000008) GPIO_OUT <= GPIO_OE & wr_data[27:0] ;
    end
end
endmodule