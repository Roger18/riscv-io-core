`ifndef PICORV32_REGS
`define PICORV32_REGS picosoc_regs
`endif

`ifndef PICOSOC_MEM
`define PICOSOC_MEM picosoc_mem
`endif

`ifndef PICOSOC_STACK
`define PICOSOC_STACK picosoc_stack
`endif

`define BVFSOC_V
module bvf_soc (
    input             clk,
    input             resetn,
    input      [27:0] GPIO_IN,
	
    output     [27:0] GPIO_OE,
    output     [27:0] GPIO_OUT 
);
    parameter [ 0:0] BARREL_SHIFTER = 1;
	parameter [ 0:0] ENABLE_MUL = 1;
	parameter [ 0:0] ENABLE_DIV = 1;
	parameter [ 0:0] ENABLE_FAST_MUL = 1;
	parameter [ 0:0] ENABLE_COMPRESSED = 1;
	parameter [ 0:0] ENABLE_COUNTERS = 1;

	parameter integer MEM_WORDS = 1024;
	parameter [31:0] RAMADDR = 32'h 0010_0000;
	parameter [31:0] STACKADDR = RAMADDR + (4*MEM_WORDS);       // end of memory
	parameter integer STACK_SIZE = 1024;
	parameter [31:0] PROGADDR_RESET = 32'h 0000_0000;
	parameter [31:0] IO_ADDR = 32'h 0010_2000;


    wire [ 0:0] io_valid;
    wire [ 0:0] io_ready;
    wire [31:0] IO_READ_DATA;

    wire [ 0:0] mem_valid;
    wire [ 0:0] mem_instr;
    wire [ 0:0] mem_ready;
    wire [31:0] mem_addr;
    wire [31:0] mem_wdata;  
    wire [ 3:0] mem_wstrb;
    wire [31:0] mem_rdata;

	wire [ 0:0] ram_ready;
	wire [ 9:0] ram_addr;
	wire [ 3:0] ram_valid;
	wire [31:0] ram_rdata;

	wire [ 3:0] stack_valid;
	wire [ 0:0] stack_ready;
	wire [ 9:0] stack_addr;
	wire [31:0] stack_rdata;

	wire [ 0:0] instr_valid;
	wire [ 0:0] instr_ready;
	wire [19:0] instr_addr;
	wire [31:0] instr;
 // --------------------------------------------------------------------------------------------------------------------

assign ram_ready = (mem_valid && !mem_instr &&!mem_ready && mem_addr < STACKADDR && mem_addr >= RAMADDR);
assign ram_valid = (mem_valid && !mem_ready && mem_addr < STACKADDR && mem_addr >= RAMADDR) ? mem_wstrb : 4'b0;
assign ram_addr = mem_addr[11:2];

assign stack_ready = (mem_valid && !mem_instr && !mem_ready && mem_addr >= STACKADDR && mem_addr < IO_ADDR);
assign stack_valid = (mem_valid && !mem_ready && mem_addr >= STACKADDR && mem_addr < IO_ADDR) ? mem_wstrb : 4'b0;
assign stack_addr = mem_addr[11:2];

assign instr_valid = (mem_valid && mem_instr && mem_addr < RAMADDR && mem_wstrb == 0) ? 1 : 0;
assign instr_ready = (mem_valid && mem_instr && mem_addr < RAMADDR);
assign instr_addr = mem_addr[21:2];

assign io_valid = (mem_valid && !mem_instr && !mem_ready && mem_addr >= IO_ADDR );

assign mem_ready = (io_ready || ram_ready || stack_ready || instr_ready);
assign mem_rdata = (io_ready) ? IO_READ_DATA : (ram_ready) ? ram_rdata : (stack_ready) ? stack_rdata : (instr_ready) ? instr : 0;
// -----------------------------------------------------------------------------------------------------------------------

// Reg-mapped IO controller for BeagleV-Fire
IO_controller #(
    .IO_ADDR(IO_ADDR)
) 
io(
    .clk           (clk           ), 
    .io_valid      (io_valid      ),
    .io_addr       (mem_addr      ), 
    .IO_WRITE_DATA (mem_wdata     ),
    .io_ready      (io_ready      ),
    .GPIO_IN       (GPIO_IN       ),
    .GPIO_OE       (GPIO_OE       ),
    .GPIO_OUT      (GPIO_OUT      ),
    .IO_READ_DATA  (IO_READ_DATA  )
);

// PicoRV CPU instantiation
picorv32 #(
		.STACKADDR(STACKADDR),
		.PROGADDR_RESET(PROGADDR_RESET),
		.BARREL_SHIFTER(BARREL_SHIFTER),
		.COMPRESSED_ISA(ENABLE_COMPRESSED),
		.ENABLE_COUNTERS(ENABLE_COUNTERS),
		.ENABLE_MUL(ENABLE_MUL),
		.ENABLE_DIV(ENABLE_DIV),
		.ENABLE_FAST_MUL(ENABLE_FAST_MUL),
		.ENABLE_IRQ(0)
	) cpu (
		.clk         (clk        ),
		.resetn      (resetn     ),
		.mem_valid   (mem_valid  ),
		.mem_instr   (mem_instr  ),
		.mem_ready   (mem_ready  ),
		.mem_addr    (mem_addr   ),
		.mem_wdata   (mem_wdata  ),
		.mem_wstrb   (mem_wstrb  ),
		.mem_rdata   (mem_rdata  )
	);

//Data Memory instantiation
`PICOSOC_MEM #(
	.MEM_WORDS(MEM_WORDS)
) soc_mem (
	.clk   (clk            ),
	.wen   (ram_valid      ),
	.addr  (ram_addr       ),
	.wdata (mem_wdata      ),
	.rdata (ram_rdata      ) 
);

//Stack Memory instanciation
`PICOSOC_STACK #(
	.STACK_SIZE(STACK_SIZE)
) soc_stack (
	.clk   (clk            ),
	.wen   (stack_valid    ),
	.addr  (stack_addr     ),
	.wdata (mem_wdata      ),
	.rdata (stack_rdata    ) 
);

program_mem inst_mem(
	.instr_addr  (instr_addr ),
	.instr       (instr      ),
	.instr_valid (instr_valid)
);

endmodule

// --------------------------------------------------------------------------------------------------------------------------------------

// This module can be updated if the register file needs to be used from SRAM and commenting definition line in picorv32.v
// The format used for SRAM wrapper must be same as below module.
module picosoc_regs (
	input clk, 
	input wen,
	input [5:0] waddr,
	input [5:0] raddr1,
	input [5:0] raddr2,
	input [31:0] wdata,
	output [31:0] rdata1,
	output [31:0] rdata2
);
	reg [31:0] regs [0:31];

	always @(posedge clk)
		if (wen) regs[waddr[4:0]] <= wdata;

	assign rdata1 = regs[raddr1[4:0]];
	assign rdata2 = regs[raddr2[4:0]];
endmodule

// This module will be updated by SRAM wrapper to use on-board memory as scratchpad memory(RAM)
module picosoc_mem #(
	parameter integer MEM_WORDS = 1024
) (
	input clk,
	input [3:0] wen,
	input [9:0] addr,
	input [31:0] wdata,
	output reg [31:0] rdata
);
	reg [31:0] mem [0:MEM_WORDS-1];

	always @(posedge clk) begin
		rdata <= mem[addr];
		if (wen[0]) mem[addr][ 7: 0] <= wdata[ 7: 0];
		if (wen[1]) mem[addr][15: 8] <= wdata[15: 8];
		if (wen[2]) mem[addr][23:16] <= wdata[23:16];
		if (wen[3]) mem[addr][31:24] <= wdata[31:24];
	end
endmodule

//This module can be replaced by SRAM wrapper to use on-board memory as stack
module picosoc_stack #(
	parameter integer STACK_SIZE = 1024
) (
	input clk,
	input [3:0] wen,
	input [9:0] addr,
	input [31:0] wdata,
	output reg [31:0] rdata
);
	reg [31:0] mem [0:STACK_SIZE-1];

	always @(posedge clk) begin
		rdata <= mem[addr];
		if (wen[0]) mem[addr][ 7: 0] <= wdata[ 7: 0];
		if (wen[1]) mem[addr][15: 8] <= wdata[15: 8];
		if (wen[2]) mem[addr][23:16] <= wdata[23:16];
		if (wen[3]) mem[addr][31:24] <= wdata[31:24];
	end
endmodule
